planned for after 1.0
- accurate nav slider after new moves played (base on nextSibling)
- copy/paste ascii diagrams (http://www.dse.nl/~toni/go/ascii2sgf/index_uk.htm)
- edit game info
- show more search results
- tree navigation
- multiple games per sgf
- make failed game load non-fatal
- show continuations for pattern search
- game archive browsing/searching
- brief help text (keyboard shortcuts, etc)?
- make tools selection prettier
- yield during load for more responsive ui
- KJD permalinks that only require one JSON request
- exhaustive FF[4] SGF support

maybe
- improve overall speed (rendering, capturing, history, etc)
- full-featured flash renderer
- subclass Array and leave no trace (each player has own iframe?)
- shortcuts only when Player has focus
- fix safari 2 history
- native iPhone app
- IGS client
- more robust SGF parser
