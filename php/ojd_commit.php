<?php

require("db.php");
require("json.php");
require("sgf.php");

mysql_connect(DB_HOST, DB_USER, DB_PASS);
mysql_select_db(DB_NAME);

$id = (int)$_REQUEST['id'];
$parent = (int)$_REQUEST['parent'];
if (!$id && !$parent) {
	die("Missing id.");
}
if ($id == 1) {
	die("Editing root node is not allowed.");
}


# SGF-ish string, but with no variations and meant for single tree node. Get array of SGF nodes.
# TODO: Perform basic filtering; esp. disallow changes of B[] and W[] during updates!
function parse_simple_sgf($str)
{
	$sgf = new SGF("(;".$str.")");
	return $sgf->tree['trees'][0]['nodes'][1];
}

$sgf_nodes = parse_simple_sgf($_REQUEST['sgf']);


# 1. tree_node_archive
# 2. sgf_node
# 3. tree_node

mysql_query('START TRANSACTION');


if (!mysql_query("INSERT INTO tree_node_archive SET tree_pos = '".($id?$id:1)."', author = '".$_SERVER['REMOTE_ADDR']."'")) {
	die("Problem! " . mysql_error());
}
$version = mysql_insert_id();


function sgf_node_add($version, $code, $value)
{
	if (!mysql_query("INSERT INTO sgf_node SET tnode = $version, code = '" . mysql_real_escape_string($code) . "', value = '" . mysql_real_escape_string($value) . "'")) {
		die("Problem! " . mysql_error());
	}
}

foreach ($sgf_nodes as $code => $value) {
	if (is_array($value)) {
		foreach ($value as $val1) {
			sgf_node_add($version, $code, $val1);
		}
	} else {
		sgf_node_add($version, $code, $value);
	}
}


if ($id) {
	if (!mysql_query("UPDATE tree_node SET version = $version WHERE id = $id")) {
		die("Problem! " . mysql_error());
	}
} else {
	if (!mysql_query("INSERT INTO tree_node SET version = $version, parent = $parent")) {
		die("Problem! " . mysql_error());
	}
	$id = mysql_insert_id();
	if (!mysql_query("UPDATE tree_node_archive SET tree_pos = $id WHERE id = $version")) {
		die("Problem! " . mysql_error());
	}
}

mysql_query('COMMIT');


$json = new Services_JSON();
echo $json->encode(array(
		"id"	=> $id,
));

?>
