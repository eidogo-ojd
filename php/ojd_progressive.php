<?php

require("db.php");
require("json.php");

mysql_connect(DB_HOST, DB_USER, DB_PASS);
mysql_select_db(DB_NAME);

$sgf_start = array();

$id = (int)$_REQUEST['id'];
if (!$id) {
	$id = 0; // show first tree by default
	$sgf_start = array(
		'GM' => 1, # Go
		'FF' => 3, # SGFv3
		'VW' => null, # whole board visible
		'SO' => 'The Depths of Internet', # source
		'EV' => null, # event
		'PC' => null, # place
		'SZ' => 19, # board size
		'HA' => 0, # no handi
		'ST' => 0, # variation display contorl
		'DT' => null, # date
		'KM' => 0, # komi
		'GN' => 'Open Joseki Dictionary (until someone thinks of a cooler name)',
		'CP' => 'Public Domain for now',
		'GC' => 'Surely this is gonna be the next big thing in Go!');
}


function get_sgf_nodes($version)
{
	$sgf_query = mysql_query("SELECT code, value FROM sgf_node WHERE tnode = '$version'");
	$nodes = array();
	while ($node = mysql_fetch_assoc($sgf_query)) {
		if ($nodes[$node['code']]) {
			if (is_array($nodes[$node['code']])) {
				array_push($nodes[$node['code']], $node['value']);
			} else {
				$nodes[$node['code']] = array($nodes[$node['code']], $node['value']);
			}
		} else {
			$nodes[$node['code']] = $node['value'];
		}
	}
	return $nodes;
}


$tree_query = mysql_query("SELECT tn.id AS id, tn.parent AS parent, tna.id AS version
	FROM tree_node AS tn
	LEFT JOIN tree_node_archive AS tna ON tna.id = tn.version
	WHERE IFNULL(tn.parent, 0) = '".mysql_real_escape_string($id)."'");
$trees = array();
if (!$tree_query) {
	echo "Error loading game data.";
} else {
	while ($node = mysql_fetch_assoc($tree_query)) {
		$tree['id'] = $node['id'];
		$tree['parent'] = $node['parent']; if ($tree['parent'] == null) $tree['parent'] = 0;
		$tree['nodes'] = array(array_merge($sgf_start, get_sgf_nodes($node['version'])));
		$tree['trees'] = array();
		$trees[] = $tree;
	}
	$json = new Services_JSON();
    echo $json->encode(array(
		"id"	=> $id,
		"nodes"	=> array(),
		"trees"	=> $trees,
	));
}

?>
