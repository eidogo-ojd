<table>
<?php

require("db.php");
require("sgf.php");

mysql_connect(DB_HOST, DB_USER, DB_PASS);
mysql_select_db(DB_NAME);


function get_moves_path($tpos)
{
	$tree_query = mysql_query("SELECT tn.parent AS parent, IFNULL(sgf.value, '?') AS coord FROM tree_node AS tn LEFT JOIN sgf_node AS sgf ON sgf.tnode = tn.version AND (sgf.code = 'B' OR sgf.code = 'W') WHERE tn.id = $tpos");
	$tnode = mysql_fetch_assoc($tree_query);
	if ($tnode['parent']) {
		$str = get_moves_path($tnode['parent']) . ' ';
	} else {
		$str = '';
	}
	$str .= sgfpos2coord($tnode['coord']);
	return $str;
}


$changes_query = mysql_query("SELECT tna.id AS version, tna.tree_pos AS tree_pos, tna.ctime AS ctime, tna.author AS author FROM tree_node_archive AS tna ORDER BY tna.id DESC LIMIT 100");
while ($change = mysql_fetch_assoc($changes_query)) {
	echo "<tr><td>".$change['version']."</td><td>".$change['ctime']."</td><td>".$change['author']."</td><td>".get_moves_path($change['tree_pos'])."</td></tr>\n";
}

?>
</table>
