# The main variations tree lives in table tree_node, referencing the current
# version of the nodes in tree_node_archive. Each node has a set of SGF nodes
# (sgf_node), set of attributed comments (node_comment), editorial comment
# and possibly other metadata later.


DROP TABLE sgf_node;
DROP TABLE tree_node;
DROP TABLE tree_node_archive;


CREATE TABLE tree_node_archive (
	id INT AUTO_INCREMENT PRIMARY KEY,
	tree_pos INT NOT NULL,
	FOREIGN KEY (tree_pos) REFERENCES tree_node (id),

	ctime DATETIME NOT NULL,
	# Also IPv6 addresses and other junk!
	author VARCHAR(128) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE tree_node (
	id INT AUTO_INCREMENT PRIMARY KEY,
	parent INT DEFAULT NULL,
	FOREIGN KEY (parent) REFERENCES tree_node (id) ON DELETE CASCADE,

	version INT NOT NULL,
	FOREIGN KEY (version) REFERENCES tree_node_archive (id)
) ENGINE=InnoDB;


# Board-related SGF node
# This should be used only for trivial SGF nodes; any metadata should be
# stored in proper table columns.
CREATE TABLE sgf_node (
	id INT AUTO_INCREMENT PRIMARY KEY,

	tnode INT NOT NULL,
	FOREIGN KEY (tnode) REFERENCES tree_node_archive (id) ON DELETE CASCADE,

	code VARCHAR(2) NOT NULL,
	value TEXT NOT NULL
) ENGINE=InnoDB;


INSERT INTO tree_node_archive SET editorial="The Road Begins Here";
INSERT INTO tree_node SET version=1;
